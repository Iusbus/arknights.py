# Installation

Clone the git or download the zip from the site

```
    $ git clone https://github.com/Iusbus/Python-Scripts.git
```

# General Usage

For both bash (Linux, MacOS, WSL) and powershell/cmd (Windows), you just have to install python, and supply the script to it.

## Auto completion

Just as a note, I've done auto completion (or tab completion) from bash side for myself. I couldn't be bothered to learn the argcomplete library since I already knew how to do completion for bash.
I will admit that usage for pretty much any script is pretty arse without auto completion, but unless I'm extremely bored some evening, ain't happening.

<br />

# Arknights scripts

<details><summary>planner.py</summary><br />

<ul><details><summary>Required libraries</summary><br />

Please ensure that all of these are installed (some might have come with your python installation). Check the documentation for the libraries for any additional requirements

> argparse <br />
> csv <br />
> pyperclip <br />
> os <br />
> sys <br />

</details></ul><br />

This is mostly meant to be used as a companion tool for the operator planner tool made by [Gamepress](https://gamepress.gg/arknights/tools/arknights-operator-planner-beta#).
It's used to make keeping track of what you have much easier, and also keeping that inventory in a format that the tool accepts.

## Usage

### Main usage

```
    $ python planner.py ITEM AMOUNT [ITEM AMOUNT ...]
```

The script uses a CSV file as its target. The default path is the scripts directory, and it will create a file for itself if it can't find it.  
The script takes in arguments in pairs, and can take as many at once as you put in. It then prints out the new amount for any items you changed.

### Crafting mode

```
    $ python planner.py [-c, --craft] RECIPE AMOUNT
```

You can use the --craft flag to save you some typing.
For example:

```
    $ python planner.py -c d2 5
```

Would add 5 devices and remove 15 damaged devices from your inventory

### Additional flags

```
    -l, --list  Enables list mode, allowing you to list items without writing anything.
    -x, --copy  At the end of operation, copies the CSV to clipboard. Can also be used alone or with --list to just copy the CSV.
```

### Examples

Assuming file is not found, adding integrated devices with short name

```
     $ python planner.py d3 25
     Warning: file not found, creating one in /home/iusbus/dev/python/arknights/planner.csv
     Integrated Device : 25
     Success: CSV is ready
```

Assuming file is found, removing integrated devices with long name

```
    $ python planner.py integrateddevice -10
    Integrated Device : 15
    Success: CSV is ready
```

</details>

<details><summary>sanity.py</summary><br />

<ul><details><summary>Required libraries</summary><br />

Please ensure that all of these are installed (some might have come with your python installation). Check the documentation for the libraries for any additional requirements

> argparse <br />
> datetime <br />

</details></ul><br />

When I first wanted to create the planner.py script, I had absolutely zero experience in python, so I made this as practice really.  
All it does is tell you the time when your sanity is full. No arguments have to be supplied, in which case the script just tells you the time when your sanity would reach the default maximum of 135 from 0.

## Usage

```
    $ python sanity.py [CURRENT_SANITY] [MAX_SANITY] [MISSION_COST]
```

</details>

<details><summary>recruit.py</summary><br />

<ul><details><summary>Required libraries</summary><br />

Please ensure that all of these are installed (some might have come with your python installation). Check the documentation for the libraries for any additional requirements

> argparse <br />
> itertools <br />
> sys <br />

</details></ul><br />
This was definitely out of boredom rather than necessity, since this near perfect [tool by Aceship and Faryzal20202](https://aceship.github.io/AN-EN-Tags/akhr.html) exists. However, with this I can use autocompletion from bash, which does make this marginally easier and quicker for me to use. And you know, not having to load a website and just launch terminal is nice.

## Usage

### Main usage

```
    $ python recruit.py TAG [TAG ...]
```

Lists any 4+ star operators you are guaranteed to get. If you supply only one tag, it will instead print out every operator with that tag (apart from 6 stars, unless top operator was given). The script looks through combinations, meaning you can supply all of the tags you are given. For example:

```
    $ python recruit.py top_operator nuker defender ranged medic

     Tags that guarantee a specific 6 star operator:
    With tags Top Operator + Nuker - Ch'en
```

### Show mode

```
    $ python recruit.py [-s, --show] TAG [TAG ... ]
```

Also shows each operator you can get with the given tags under any possible guarantee 4+ stars

### Look up feature

```
    $ python recruit.py [-l, --list] OPERATOR [OPERATOR ...]
```

You can also look up any operators tags, so if you have any specific operators you want, you can look up their tags to watch for in the future.

</details>

<details><summary>headhunt.py</summary><br />

<ul><details><summary>Required libraries</summary><br />

Please ensure that all of these are installed (some might have come with your
python installation). Check the documentation for the libraries
for any additional requirements

> argparse <br />
> calendar <br />
> datetime <br />
> sys <br />

</details></ul><br />
This script allows you to look ahead when planning your pulls.
The script does not take into account certificate shops or new annihilations, but
everything from daily/weekly quests, monthly free ticket,

## Usage

### Main usage

```
    $ python headhunt.py ORIGINITE ORUNDUM TICKETS MONTHLY [DATE]
```

Supply originium, orundum and the tickets you will be spending on the banner.
The monthly argument is a toggle (True or 1/False or 0) and adds 200 orundum
daily from the monthly card purchase.

Date is formatted in YY/MM/DD, with default being current system date, so for
example:

14th of December 2022 > 22/12/14

### The math

The script counts days until the given date, and mondays in that time period.

Monday is reset day, so it is used to count weeks when calculating
orundum from weekly quests and weekly annihilation.

Weekly gains are 2300, daily gains are 100 or 300 with monthly card.  
A free ticket is given on the seventeeth of the month.

Resulting:

```
    (Originium/180 + Orundum + Mondays*2300 + Days*(100 or 300))/600 + 17ths
    + Tickets
```

</details>
