import argparse
from datetime import datetime, timedelta

parser = argparse.ArgumentParser(epilog='Prints out the time when your sanity reaches full. The time is accurate to ±6 minutes, and will stay accurate if your sanity cost input will put current sanity below zero.')
parser.add_argument("curSanity", help="Your current sanity. Defaults to 0", nargs='?', default=0, type=int)
parser.add_argument("maxSanity", help="Target sanity to regen for. Defaults to 130", nargs='?', default=130, type=int)
parser.add_argument("sanityCost", help="Sanity cost for a mission you plan on doing. Deducts from current sanity and defaults to 0", nargs='?', default=0, type=int)

args = parser.parse_args()

# If no arguments are set, everything zeroes out. This fixes that
if not args.maxSanity:
	args.maxSanity = 130

hours = (args.maxSanity - (args.curSanity - args.sanityCost))/10
timeToFull = (datetime.now() + timedelta(hours=hours))

tests = (
        int(datetime.now().strftime('%d')) < int(timeToFull.strftime('%d')),            # Simplest case when no month bullshit is going on
        int(datetime.now().strftime('%m')) < int(timeToFull.strftime('%m')),            # Edge case when using on the last day of the month
        int(datetime.now().strftime('%m')) == 12 and int(timeToFull.strftime('%m')) == 1 # Edge case when using during 31st of December
        )

if any(tests):
    dayTense = 'tomorrow'
else:
    dayTense = 'today'

print ('Sanity reaches',args.maxSanity,dayTense,'at',timeToFull.strftime('%H:%M'))
