import argparse
import csv
import os
import sys
from lists import itemList, recipes

parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument(
    "items",
    nargs="*",
    help="ITEM QUANTITY [ITEM QUANTITY...]\nAdds/deducts supplied quantity/quantities from supplied item/items",
    default=["all"],
)
parser.add_argument(
    "-l",
    "--list",
    action="store_true",
    dest="listMode",
    default=False,
    help="Enables list mode, allowing you to list items without writing anything",
)
parser.add_argument(
    "-c",
    "--craft",
    action="store_true",
    dest="craftingMode",
    default=False,
    help="Enables crafting mode, allowing you to easily manage your inventory when crafting",
)
parser.add_argument(
    "-t",
    "--total",
    action="store_true",
    dest="totalMode",
    default=False,
    help="Instead of adding/deducting from the csv entries, change the entries to given amounts",
)
try:
    import pyperclip
    hasPyperclip=True
    parser.add_argument(
        "-x",
        "--copy",
        action="store_true",
        dest="copyFlag",
        default=False,
        help="At the end of operation, copies the CSV to clipboard. Can also be used alone or with --list to just copy the CSV",
    )
except ImportError:
    hasPyperclip=False
    pass
args = parser.parse_args()

# Crafting mode already prints out the end result, no point in listing.
# Crafting mode and Total mode don't make sense together, prefer crafting.
if args.craftingMode and (args.listMode or args.totalMode):
    args.listMode = False
    args.totalMode = False

readableList = [a[0] for a in itemList]


def grouped(iterable, n):
    """Concatenate lists into groups of n.

    Example: [a, b, c, d] -> [[a, b], [c, d]]
    """
    return zip(*[iter(iterable)] * n)


# Directory for the running script
folder = os.path.dirname(os.path.realpath(__file__))

# Create CSV if it doesn't exist already
try:
    open(folder + "/planner.csv")
except IOError:
    print("Warning: file not found, creating one in", folder + "/planner.csv")
    with open(folder + "/planner.csv", "w", newline="") as file:
        for item in readableList:
            csv.writer(file, delimiter=",").writerow([item, 0])
        file.close()


def turnReadable(itemInput):
    """Turns inputs into a human readable format. """
    if itemInput not in readableList:
        for x in itemList:
            if itemInput in x:
                itemInput = x[0]
                return itemInput
    return itemInput


if args.listMode:
    for x in args.items:
        with open(folder + "/planner.csv") as file:
            reader = csv.reader(file.readlines())
            if len(args.items) == 1 and args.items[0] == "all":
                for row in reader:
                    print(row[0], ": ", row[1], sep="")
            else:
                for row in reader:
                    for item in args.items:
                        if row[0] == turnReadable(item):
                            print(row[0], ": ", row[1], sep="")
        if not args.copyFlag:
            sys.exit()

if hasPyperclip and (
    args.copyFlag and args.items[0] == "all"
):  # The 'all' is a default state when no arguments are supplied
    with open(folder + "/planner.csv") as file:
        text = file.read()
        if not text[
            -1:
        ].isdigit():  # This stupid shit is because csv.writer always inserts a final linebreak, but I can't be sure if it does that with every environment 'cause I'm too lazy to test
            pyperclip.copy(text[:-1])
        else:
            pyperclip.copy(text)
    print("Success: CSV copied")
    sys.exit()

# Error checking, exit on failure so the actual main part doesn't wipe the file
argList = []
if args.craftingMode and len(args.items) != 2:
    print("Error: Crafting mode requires exactly two arguments")
    sys.exit()
if len(args.items) % 2 != 0:
    print("Error: Odd number of arguments")
    sys.exit()
for x, y in grouped(args.items, 2):
    argList.append(x)
    if turnReadable(x) not in readableList:
        print("Error: [", x, "] not in list", sep="")
        sys.exit()
    try:
        y = int(y)
    except:
        print("Error: Wrong argument types in pair [", x, ",", y, "]", sep="")
        sys.exit()
if len(argList) != len(set(argList)):
    print("Error: Duplicate arguments")
    sys.exit()

if args.craftingMode:
    if turnReadable(args.items[0]) not in recipes:
        print(turnReadable(args.items[0]), "is not a craftable item")
        sys.exit()
    for a in recipes[turnReadable(args.items[0])]:
        args.items.append(a)
        args.items.append(
            int(recipes[turnReadable(args.items[0])][a]) * int(args.items[1])
        )
    if "Chip" in turnReadable(args.items[0]):
        args.items[1] = int(args.items[1]) * 2

# Main script, I still have no idea how this works, but it does, so I'm not gonna touch it
with open(folder + "/planner.csv") as file:
    reader = csv.reader(file.readlines())
    newList = []
    for row in reader:
        for x, y in grouped(args.items, 2):
            if turnReadable(x) == row[0]:
                if args.totalMode:
                    row[1] = int(y)
                else:
                    row[1] = int(row[1]) + int(y)
                if row[1] < 0:
                    print("Error: [", row[0], "] would go negative", sep="")
                    sys.exit()
                if row not in newList:
                    newList.append(row)
                print(row[0], ": ", row[1], sep="")
            else:
                if row not in newList:
                    newList.append(row)
    file.close()
    with open(folder + "/planner.csv", "w", newline="") as writer:
        for row in newList:
            csv.writer(writer, delimiter=",").writerow(row)
        writer.close()

if len(newList) == len(
    itemList
):  # We know the amount of rows the CSV should have, so just check against that
    print("Success: CSV is ready")
    if hasPyperclip and args.copyFlag:
        with open(folder + "/planner.csv") as file:
            text = file.read()
            if not text[
                -1:
            ].isdigit():  # This stupid shit is because csv.writer always inserts a final linebreak, but I can't be sure if it does that with every environment 'cause I'm too lazy to test
                pyperclip.copy(text[:-1])
            else:
                pyperclip.copy(text)
else:
    print("CSV rows don't match the item count, check file integrity")
